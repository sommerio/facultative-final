
INSERT INTO course(id, name, start, end, level)
VALUES (1, 'JAVA', '2021-12-01', '2022-03-30', 'beginner'),
       (2, 'HTML/CSS', '2022-01-01', '2022-05-30', 'advanced'),
       (3, 'Python', '2022-03-01', '2022-06-28', 'intermediate'),
       (4, 'C# beginners', '2022-02-01', '2022-07-31', 'beginner'),
       (5, 'C/C++', '2021-12-01', '2022-03-30', 'advanced'),
       (6, 'NodeJS', '2021-12-01', '2022-03-31', 'intermediate');


INSERT INTO instructor (id, name, phone, email, class)
VALUES (1, 'Ryan Dahl', '+1 543 71 23 45', 'dahl@course.com', 6),
       (2, 'Guido van Rossum', '+1 987 002 12 33', 'rossum@course.com',3),
       (3, 'Bjarne Stroustrup', '+1 765 786 77 77', 'stroustrup@course.com', 5),
       (4, 'Anders Hejlsberg', '+21 123 673 67 67', 'hejlsberg@course.com', 4),
       (5, 'James Gosling', '+2 123 443 55', 'gosling@course.com', 1),
       (6, 'Jon Duckett', '+7 654 321 00 00', 'duckett@course.com', 2);
