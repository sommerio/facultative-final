

CREATE TABLE IF NOT EXISTS course
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY,
    name       VARCHAR(255) NOT NULL,
    start      DATE,
    end        DATE,
    level      VARCHAR(25)
);

CREATE TABLE IF NOT EXISTS instructor
(
    id         BIGINT AUTO_INCREMENT PRIMARY KEY,
    name       VARCHAR (50) NOT NULL,
    phone      VARCHAR (50),
    email      VARCHAR (50),
    class      BIGINT,
    FOREIGN KEY (class) REFERENCES course(ID) ON DELETE SET NULL
);

