package com.epam.project.persistence;

import java.sql.SQLException;
import java.util.List;

public interface CrudDao<T> {

    T get(Long id) throws SQLException;
    List<T> getAll() throws SQLException;
    Boolean create(T obj) throws SQLException;
    Boolean update(T obj) throws SQLException;
    Boolean delete(Long id) throws SQLException;
}
