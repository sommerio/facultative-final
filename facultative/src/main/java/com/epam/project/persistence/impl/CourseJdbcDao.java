package com.epam.project.persistence.impl;

import com.epam.project.entity.Course;
import com.epam.project.entity.Level;
import com.epam.project.persistence.CourseDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CourseJdbcDao implements CourseDao {

    private Connection connection;


    public CourseJdbcDao(Connection connection) {
        this.connection = connection;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseJdbcDao.class);


    @Override
    public List<Course> getAll() throws SQLException {
        List<Course> courses = new ArrayList<>();
        PreparedStatement pr = connection.prepareStatement("select * from course");
        ResultSet resultSet = pr.executeQuery();
        while (resultSet.next()) {
            Course course = mapRow(resultSet);
            courses.add(course);
        }
        return courses;
    }

    @Override
    public Course get(Long id) throws SQLException {
        Course course = new Course();
        PreparedStatement pr = connection.prepareStatement("select * from course where id=?");
        pr.setLong(1, id);
        ResultSet resultSet = pr.executeQuery();
        while (resultSet.next()) {
            course = mapRow(resultSet);
        }
        return course;
    }


    @Override
    public Boolean create(Course course) throws SQLException {
        PreparedStatement pr = connection.prepareStatement("insert into course (name, start, end, level) values (?, ?, ?, ?)");

        pr.setString(1, course.getName());
        if (course.getStart() == null) {
            pr.setDate(2,null);

        } else {
            pr.setDate(2,Date.valueOf(course.getStart()));
        }
        if (course.getEnd() == null) {
            pr.setDate(3,null);

        } else {
            pr.setDate(3,Date.valueOf(course.getEnd()));
        }
        if (course.getLevel() == null) {
            pr.setString(4, null);

        } else {
            pr.setString(4, course.getLevel().toString());
        }
        int result = pr.executeUpdate();
        return result > 0;
    }

    @Override
    public Boolean delete(Long id) throws SQLException {
        PreparedStatement pr = connection.prepareStatement("delete from course where id=?");
        pr.setLong(1, id);
        int res = pr.executeUpdate();
        return res > 0;
    }
    @Override
    public Boolean update(Course course) throws SQLException {
        PreparedStatement pr = connection.prepareStatement("update course set name=?, start=?, end=?, level=? where id=?");
        pr.setString(1, course.getName());
        if (course.getStart() != null) {
            pr.setDate(2,Date.valueOf(course.getStart()));
        } else {
            pr.setDate(2, null);
        }
        if (course.getEnd() != null){
            pr.setDate(3,Date.valueOf(course.getEnd()));
        } else {
            pr.setDate(3, null);
        }
        if (course.getLevel() != null) {
            pr.setString(4, course.getLevel().toString());
        } else {
            pr.setString(4, null);
        }
        pr.setLong(5, course.getId());
        int result = pr.executeUpdate();
        return result > 0;
    }


    private Course mapRow(ResultSet resultSet) throws SQLException {
        Course course = new Course();

        course.setId(resultSet.getLong("id"));
        course.setName(resultSet.getString("name"));
        if (resultSet.getDate("start") == null) {
            course.setStart(null);
        } else {
            course.setStart(resultSet.getDate("start").toLocalDate());
        }
        if (resultSet.getDate("end") == null) {
            course.setEnd(null);
        } else {
            course.setEnd(resultSet.getDate("end").toLocalDate());
        }
        if (resultSet.getString("level") == null) {
            course.setLevel(null);
        } else {
            course.setLevel(Level.valueOf(resultSet.getString("level").toUpperCase()));
        }


        return course;

    }

}
