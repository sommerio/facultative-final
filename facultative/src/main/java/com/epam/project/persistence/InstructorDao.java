package com.epam.project.persistence;

import com.epam.project.entity.Course;
import com.epam.project.entity.Instructor;

import java.sql.SQLException;
import java.util.List;

public interface InstructorDao extends CrudDao<Instructor>{

    Instructor getInstructorByCourseName(String courseName);
    List<Course> getAllCoursesByInstructorId(Long instructorId);


}
