package com.epam.project.persistence.impl;

import com.epam.project.Datasource;
import com.epam.project.entity.Course;
import com.epam.project.entity.Instructor;
import com.epam.project.persistence.InstructorDao;

import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InstructorJdbcDao implements InstructorDao {

    private Connection connection;


    public InstructorJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Instructor get(Long id) throws SQLException {
        Instructor instructor = new Instructor();
        PreparedStatement pr = connection.prepareStatement("select * from instructor where id=?");
        pr.setLong(1, id);
        ResultSet resultSet = pr.executeQuery();
        while (resultSet.next()) {
            instructor = mapRow(resultSet);
        }
        return instructor;
    }

    @Override
    public List<Instructor> getAll() throws SQLException {
        List<Instructor> instructors = new ArrayList<>();
        PreparedStatement pr = connection.prepareStatement("select * from instructor");
        ResultSet resultSet = pr.executeQuery();
        while (resultSet.next()) {
            Instructor instructor = mapRow(resultSet);
            instructors.add(instructor);
        }
        return instructors;
    }

    @Override
    public Boolean create(Instructor instructor) throws SQLException {
        PreparedStatement pr = connection.prepareStatement("insert into instructor (name, phone, email, class) values (?, ?, ?, ?)");

        pr.setString(1, instructor.getName());
        pr.setString(2, instructor.getPhone());
        pr.setString(3, instructor.getEmail());
        pr.setLong(4, instructor.getCourse());

        int create = pr.executeUpdate();

        return create > 0;
    }

    @Override
    public Boolean update(Instructor instructor) throws SQLException {

        PreparedStatement pr = connection.prepareStatement("update instructor set name=?, phone=?, email=?, class=? where id=?");
        pr.setString(1, instructor.getName());

        pr.setString(2, instructor.getPhone());
        pr.setString(3, instructor.getEmail());
        pr.setLong(4, instructor.getCourse());
        pr.setLong(5, instructor.getId());

        int update = pr.executeUpdate();

        return update > 0;
    }

    @Override
    public Boolean delete(Long id) throws SQLException {
        PreparedStatement pr = connection.prepareStatement("delete from instructor where id=?");
        pr.setLong(1, id);
        int delete = pr.executeUpdate();

        return delete > 0;
    }

    @Override
    public List<Course> getAllCoursesByInstructorId(Long instructorId) {
        return null;
    }

    @Override
    public Instructor getInstructorByCourseName(String courseName) {
        return null;
    }




    private Instructor mapRow(ResultSet resultSet) throws SQLException {
        Instructor instructor = new Instructor();

        instructor.setId( resultSet.getLong("id"));
        instructor.setName(resultSet.getString("name"));
        instructor.setPhone(resultSet.getString("phone"));
        instructor.setEmail(resultSet.getString("email"));
        instructor.setCourse(resultSet.getLong("class"));

        return instructor;

    }

}
