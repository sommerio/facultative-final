package com.epam.project;

import com.epam.project.entity.Course;
import com.epam.project.entity.Instructor;
import com.epam.project.entity.Level;


import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;


public class App {

    private static Map<String, Integer> map = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    static Datasource tmp = Datasource.getInstance();


    static {
        map.put("Get all", 0);
        map.put("Create", 1);
        map.put("Delete", 2);
        map.put("Update", 3);
        map.put("Get", 4);
        map.put("exit", 5);

    }

    public static void main(String[] args) throws SQLException {
        App app = new App();

        boolean connectionOpen = tmp.open();
        if (connectionOpen) {
            System.out.println("App runs .... ");
            app.start();

        } else {
            System.out.println("Some DB problems occurred ...");
        }
        app.close();

    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Tables: INSTRUCTOR, COURSE");
            System.out.println("Available options are: ");
            for (String str : map.keySet()) {
                System.out.println("* " + str);
            }
            System.out.print("Select one: ");
            String input = scanner.nextLine();
            Integer key = map.get(input);
            if (key != null) {
                switch (key) {
                    case 0:
                        System.out.print("Table: ");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("course")) {
                            tmp.getAllCourses();
                        } else if (input.equalsIgnoreCase("instructor")) {
                            tmp.getAllInstructors();
                        } else {
                            System.out.println("Unknown Input");

                        }
                        break;
                    case 1:
                        System.out.print("Table: ");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("course")) {
                            System.out.print("Enter Course name (required): ");
                            String name = scanner.nextLine();
                            System.out.print("Enter Course start date: format=(YYYY-MM-DD) or type 'skip' ");
                            String begin = scanner.nextLine();
                            System.out.print("Enter Course end date: format=(YYYY-MM-DD) or type 'skip' ");
                            String end = scanner.nextLine();
                            System.out.print("Enter Course Level (Beginner, Intermediate or Advanced) or type 'skip' ");
                            String level = scanner.nextLine();
                            System.out.println();
                            createCourse(name, begin, end, level);
                        } else if (input.equalsIgnoreCase("instructor")) {
                            System.out.print("Enter Instructor Name (required): ");
                            String name = scanner.nextLine();
                            System.out.print("Enter Instructor Phone or type 'skip': ");
                            String phone = scanner.nextLine();
                            System.out.print("Enter Instructor Email or type 'skip': ");
                            String email = scanner.nextLine();
                            System.out.print("Enter Instructor valid Course ID. Required: ");
                            String courseId = scanner.nextLine();
                            createInstructor(name, phone, email, courseId);
                        } else {
                            System.out.println("Unknown Input");
                        }
                        break;
                    case 2:
                        System.out.print("Table: ");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("course")) {
                            System.out.print("Enter Course ID you want to delete: ");
                            String id = scanner.nextLine();
                            tmp.deleteCourse(Long.parseLong(id));
                        } else if (input.equalsIgnoreCase("instructor")) {
                            System.out.print("Enter Instructor ID you want to delete: ");
                            String instrId = scanner.nextLine();
                            tmp.deleteInstructor(Long.parseLong(instrId));
                        } else {
                            System.out.println("Unknown Input");

                        }
                        break;
                    case 3:
                        System.out.print("Table: ");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("course")) {
                            System.out.print("Enter Course Id you want to update: ");
                            Long id_upd = scanner.nextLong();
                            scanner.nextLine();
                            System.out.print("Enter New Course name: or type 'skip' ");
                            String _name = scanner.nextLine();
                            System.out.print("Enter course new start date: format=(YYYY-MM-DD) or type 'skip' ");
                            String _begin = scanner.nextLine();
                            System.out.print("Enter course new end date: format=(YYYY-MM-DD) or type 'skip' ");
                            String _end = scanner.nextLine();
                            System.out.print("Enter ourse new Level (Beginner, Intermediate or Advanced) or type 'skip' ");
                            String _level = scanner.nextLine();
                            updateCourse(id_upd, _name, _begin, _end, _level);
                        } else if (input.equalsIgnoreCase("instructor")) {
                            System.out.print("Enter Instructor Id you want to update: ");
                            String insId = scanner.nextLine();
                            System.out.print("Enter new name or type 'skip' ");
                            String insName = scanner.nextLine();
                            System.out.print("Enter new phone or type 'skip' ");
                            String insPhone = scanner.nextLine();
                            System.out.print("Enter new email or type 'skip' ");
                            String insEmail = scanner.nextLine();
                            System.out.print("Enter new valid Course ID or type 'skip' ");
                            String insCourseId = scanner.nextLine();
                            updateInstructor(insId, insName, insPhone, insEmail, insCourseId);
                        } else {
                            System.out.println("Unknown Input");

                        }

                        break;
                    case 4:
                        System.out.print("Table: ");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("course")) {
                            System.out.print("Enter Course ID: ");
                            Long courseId = scanner.nextLong();
                            scanner.nextLine();
                            tmp.getCourseById(courseId);
                        } else if (input.equalsIgnoreCase("instructor")) {
                            System.out.print("Enter Instructor ID: ");
                            Long insId = scanner.nextLong();
                            scanner.nextLine();
                            tmp.getInstructorById(insId);
                        } else {
                            System.out.println("Unknown Input");
                        }
                        break;
                    case 5:
                        System.out.println("App stopped. Bye!");
                        return;
                    default:
                        System.out.println("Unsupported input ");

                }
            } else {
                System.out.println("Invalid input ");
            }


        }

    }

    public void close() {
        tmp.close();
    }

    public void createCourse(String name, String begin, String end, String level) {

        Course course = new Course();
        if (name.equals("skip")) {
            return;
        }
        course.setName(name);
        if (!begin.equals("skip")) {
            course.setStart(LocalDate.parse(begin));
        } else {
            course.setStart(null);
        }
        if (!end.equals("skip")) {
            course.setEnd(LocalDate.parse(end));
        } else {
            course.setEnd(null);
        }
        if (!level.equals("skip")) {
            course.setLevel(Level.valueOf(level.toUpperCase()));
        } else {
            course.setLevel(null);
        }

        tmp.createCourse(course);

    }


    public static void updateCourse(Long id, String name, String begin, String end, String level) {
        System.out.print("Course to update: ");
        Course course = tmp.getCourseById(id);
        if (course != null) {

            if (!name.equals("skip")) {
                course.setName(name);
            }
            if (!begin.equals("skip")) {
                course.setStart(LocalDate.parse(begin));
            }
            if (!end.equals("skip")) {
                course.setEnd(LocalDate.parse(end));
            }
            if (!level.equals("skip")) {
                course.setLevel(Level.valueOf(level.toUpperCase()));
            }
           tmp.updateCourse(course);
        }

    }

    public void createInstructor(String name, String phone, String email, String courseId) {

        Instructor instructor =  new Instructor();

        instructor.setName(name);
        if (!phone.equals("skip")) {
            instructor.setPhone(phone);
        }
        if (!email.equals("skip")) {
            instructor.setEmail(email);
        }
        instructor.setCourse(Long.parseLong(courseId));

        tmp.createInstructor(instructor);

    }

    public void updateInstructor(String id, String name, String phone, String email, String courseId) {
        System.out.print("Instructor to update ");
        Instructor instructor = tmp.getInstructorById(Long.parseLong(id));
        if (instructor != null) {
            if (!name.equals("skip")) {
                instructor.setName(name);
            }
            if (!phone.equals("skip")) {
                instructor.setPhone(phone);
            }
            if (!email.equals("skip")) {
                instructor.setEmail(email);
            }
            if (!courseId.equals("skip")) {
                instructor.setCourse(Long.parseLong(courseId));
            }
            tmp.updateInstructor(instructor);
        } else {
            System.out.println("Instructor with ID=" + id + "not found");
        }

    }

}
