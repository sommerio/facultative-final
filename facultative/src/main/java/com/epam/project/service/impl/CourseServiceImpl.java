package com.epam.project.service.impl;

import com.epam.project.entity.Course;
import com.epam.project.persistence.CourseDao;
import com.epam.project.persistence.impl.CourseJdbcDao;
import com.epam.project.service.CourseService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CourseServiceImpl implements CourseService {

    private Connection connection;
    private CourseDao courseDao;

    public CourseServiceImpl(Connection connection) {
        this.connection = connection;
        courseDao = new CourseJdbcDao(connection);
    }

    @Override
    public List<Course> getAllCourses() throws SQLException {
        System.out.println("CourseServiceImpl getAllCourses() try");

        return courseDao.getAll();
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        return courseDao.delete(id);
    }

    @Override
    public Course getCourseById(Long id) throws SQLException {
        return courseDao.get(id);
    }

    @Override
    public boolean create(Course course) throws SQLException {
        return courseDao.create(course);
    }

    @Override
    public boolean update(Course course) throws SQLException {
        return courseDao.update(course);
    }
}
