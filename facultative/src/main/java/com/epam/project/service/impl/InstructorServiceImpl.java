package com.epam.project.service.impl;

import com.epam.project.Datasource;
import com.epam.project.entity.Instructor;
import com.epam.project.persistence.InstructorDao;
import com.epam.project.persistence.impl.InstructorJdbcDao;
import com.epam.project.service.InstructorService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class InstructorServiceImpl implements InstructorService {

    InstructorDao instructorDao;

    public InstructorServiceImpl(Connection connection) {
        instructorDao = new InstructorJdbcDao(connection);
    }


    @Override
    public List<Instructor> getAllInstructors() throws SQLException {
        List<Instructor> instructors = instructorDao.getAll();
        return instructors;
    }

    @Override
    public Instructor getInstructorByCourseName(String courseName) {
        Instructor instructor = instructorDao.getInstructorByCourseName(courseName);
        return null;
    }



    @Override
    public boolean create(Instructor instructor) throws SQLException {
        return instructorDao.create(instructor);
    }

    @Override
    public boolean delete(long id) throws SQLException {
        return instructorDao.delete(id);
    }

    @Override
    public boolean update(Instructor instructor) throws SQLException {
        return instructorDao.update(instructor);
    }

    @Override
    public Instructor get(Long id) throws SQLException {
        return instructorDao.get(id);
    }
}
