package com.epam.project.service;

import com.epam.project.entity.Course;

import java.sql.SQLException;
import java.util.List;

public interface CourseService{


    List<Course> getAllCourses() throws SQLException;
    Course getCourseById(Long id) throws SQLException;
    boolean update(Course course) throws SQLException;
    boolean delete(Long id) throws SQLException;
    boolean create(Course course) throws SQLException;

    //Course getByName(String name);

    // List<Course> getAllSortByStart();


}
