package com.epam.project.service;

import com.epam.project.entity.Course;
import com.epam.project.entity.Instructor;

import java.sql.SQLException;
import java.util.List;

public interface InstructorService {

    List<Instructor> getAllInstructors() throws SQLException;
    Instructor getInstructorByCourseName(String courseName);

    boolean create(Instructor instructor) throws SQLException;

    boolean delete(long id) throws SQLException;

    boolean update(Instructor instructor) throws SQLException;


    Instructor get(Long id) throws SQLException;
}


