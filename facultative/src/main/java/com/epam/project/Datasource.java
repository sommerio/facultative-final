package com.epam.project;

import com.epam.project.entity.Course;

import com.epam.project.entity.Instructor;
import com.epam.project.service.CourseService;
import com.epam.project.service.InstructorService;
import com.epam.project.service.impl.CourseServiceImpl;
import com.epam.project.service.impl.InstructorServiceImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Datasource {
    private static Datasource datasource = new Datasource();
    private  CourseService courseService;
    private  InstructorService instructorService;


    private static Connection connection = null;
    private boolean connected;


    private final static String url = "jdbc:h2:~/coursesdb";
    private final static String user = "sa";
    private final static String psw = "";


    private Datasource() {

    }

    public static Datasource getInstance() {
        if (datasource == null) {
            return new Datasource();
        }
        System.out.println("Data class "  + datasource);
        return datasource;
    }




    public boolean open() throws SQLException {
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(url, user, psw);
            System.out.println("Connected to Database.");
            instructorService = new InstructorServiceImpl(connection);
            courseService = new CourseServiceImpl(connection);

            try(Statement statement = connection.createStatement()) {
                statement.execute(getSql("createSchema.sql"));

            }
            try(Statement statement = connection.createStatement()) {
                statement.execute(getSql("insertData.sql"));
            }

            return true;

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private static String getSql(final String resourceName) {
        return new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(
                                Datasource.class.getClassLoader().getResourceAsStream(resourceName))))
                .lines()
                .collect(Collectors.joining("\n"));
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


   public void getAllCourses() {

       try {
           List<Course> courseList = courseService.getAllCourses();
           for (Course course : courseList) {
               System.out.println(course);
           }
       } catch (SQLException e) {
           System.out.println("Request failed " + e.getMessage());
       }
   }

    public void updateCourse(Course course) {
        try {
            boolean updated = courseService.update(course);
            System.out.println("updated " + updated);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
    }

    public void createCourse(Course course) {
        try {

            boolean created = courseService.create(course);
            System.out.println("created " + created);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
    }

    public void deleteCourse(Long id) {
        try {
            boolean deleted = courseService.delete(id);
            System.out.println("deleted " + deleted);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }

    }

    public Course getCourseById(Long id) {
        try {
            Course course = courseService.getCourseById(id);
            System.out.println(course);
            return course;

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
        return null;

    }

    public void getAllInstructors() {
        try {
            List<Instructor> instructors = instructorService.getAllInstructors();
            for (Instructor instructor : instructors) {
                System.out.println(instructor);
            }
        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
    }

    public void createInstructor(Instructor instructor) {
        try {
            boolean created = instructorService.create(instructor);
            System.out.println("created " + created);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
    }

    public void deleteInstructor(long id) {
        try {
            boolean deleted = instructorService.delete(id);
            System.out.println("deleted " + deleted);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }

    }

    public Instructor getInstructorById(Long id) {
        try {
            Instructor instructor = instructorService.get(id);
            System.out.println(instructor);
            return instructor;

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }
        return null;


    }

    public void updateInstructor(Instructor instructor) {
        try {
            boolean updated = instructorService.update(instructor);
            System.out.println("updated " + updated);

        } catch (SQLException e) {
            System.out.println("Request failed " + e.getMessage());
        }

    }
}

