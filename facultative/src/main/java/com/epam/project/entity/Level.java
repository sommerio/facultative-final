package com.epam.project.entity;

public enum Level {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}
