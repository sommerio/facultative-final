package com.epam.project.entity;

import java.time.LocalDate;

public class Course {

    private Long id;
    private String name;
    private LocalDate start;
    private LocalDate end;
    Level level;

    public Course() {
    }

    public Course(Long id, String name, LocalDate start, LocalDate end, Level level) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.level = level;
    }

    public Course(String name, LocalDate start, LocalDate end, Level level) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.level = level;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", level=" + level +
                '}';
    }
}
